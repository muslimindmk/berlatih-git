<?php
    require_once('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $sheep = new Animal("shaun");

echo "Nama Hewan: ". $sheep->name. "<br>"; // "shaun"
echo "Jumlah Kaki: ". $sheep->legs. "<br>"; // 4
echo "Berdarah dingin: ". $sheep->cold_blooded. "<br>"."<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");
echo "Nama Hewan: ". $kodok->name. "<br>"; // "buduk"
echo "Jumlah Kaki: ". $kodok->legs. "<br>"; // 4
echo "Berdarah dingin: ". $kodok->cold_blooded. "<br>"; // "no"
echo "Jump: ";
echo $kodok->jump()."<br>"."<br>" ; // "hop hop"

$sungokong = new Ape("kera sakti");
echo "Nama Hewan: ". $sungokong->name. "<br>"; // "sungokong"
echo "Jumlah Kaki: ". $sungokong->legs. "<br>"; // 2
echo "Berdarah dingin: ". $sungokong->cold_blooded."<br>"; // "no"
echo "Yell: ";
echo $sungokong->yell(); // "auo.."


?>