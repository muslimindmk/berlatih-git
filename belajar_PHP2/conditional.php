<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Function</title>
</head>

<body>
<h1>Berlatih Function PHP</h1>
<?php

echo "<h3> Soal No 1 Greetings </h3>";
/* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di PKS Digital School!"
*/

// Code function di sini
function greetings ($nama){
    echo "HAlo ". $nama. " Selamat Datang di PKS digital School!";
}

// Hapus komentar untuk menjalankan code!
greetings("Bagas");
echo "<br>";
greetings("Wahyu");
echo "<br>";
greetings("Abdul");
echo "<br>";

echo "<h3>Soal No 2 Reverse String</h3>";
/* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping 
(for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

// Code function di sini 
function reverse($nama1){
    $panjangnama = strlen($nama1);
    $tampung = "";
    for ($i =($panjangnama - 1); $i>=0; $i--){
        $tampung .= $nama1[$i];
    }
    return $tampung;
}

function reverseString($nama2){
    $string = reverse($nama2);
    echo $string ."<br>";
}


// Hapus komentar di bawah ini untuk jalankan Code
reverseString("abduh");
reverseString("Digital School");
reverseString("We Are PKS Digital School Developers");

echo "<h3>Soal No 3 Palindrome </h3>";
/* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari 
jawaban no.2!

*/


// Code function di sini


// PHP code to check for Palindrome string in PHP
// Recursive way using substr()
function Palindrome($word){
	
	// Base condition to end the recursive process
	if ((strlen($word) == 1) || (strlen($word) == 0)){
		echo "TRUE"."<br>";
	}

	else{
		
		// First character is compared with the last one
		if (substr($word,0,1) == substr($word,(strlen($word) - 1),1)){
			
			// Checked letters are discarded and passed for next call
			return Palindrome(substr($word,1,strlen($word) -2));
		}
		else{
			echo "FALSE"."<br>"; }
	}
}


// Hapus komentar di bawah ini untuk jalankan code
Palindrome("civic") ; // true
Palindrome("nababan") ; // true
Palindrome("jambaban"); // false
Palindrome("racecar"); // true


echo "<h3>Soal No 4 Tentukan Nilai </h3>";
/*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn 
String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika 
parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

// Code function di sini
function tentukan_nilai($angka){
    $output = "";
    if ($angka>=85 && $angka <=100){
        $output .="sangat baik";
    } else if ($angka >=70 && $angka <85){
        $output .="baik";
    } else if ($angka>=60 && $angka<70){
        $output .="cukup";
    }    else if ($angka <59){
            $output .="kurang";
    }
    return $output."<br>";
}
// Hapus komentar di bawah ini untuk jalankan code
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang


?>

</body>

</html>
